<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        //redirect
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', ['cast' => $cast]);
    }

    public function show($id)
    {
        $castbyid = DB::table('cast')->find($id);
        return view('cast.detail', ['castbyid' => $castbyid]);
    }

    public function edit($id)
    {

        $castbyid = DB::table('cast')->find($id);
        return view('cast.edit', ['castbyid' => $castbyid]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
