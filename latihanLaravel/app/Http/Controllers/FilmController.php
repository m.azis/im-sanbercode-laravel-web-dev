<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;
use Illuminate\Support\Facades\File;


class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $film = Film::all();
        return view('film.index', ["film" => $film]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $genre = Genre::all();
        return view('film.create', ["genre" => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'poster' => 'required|mimes:jpg,jpeg,png|max:2048',
            'judul' => 'required',
            'genre_id' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
        ]);

        $posterName = time() . '.' . $request->poster->extension();

        $request->poster->move(public_path('img'), $posterName);

        $film = new Film;

        $film->judul = $request['judul'];
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->genre_id = $request['genre_id'];
        $film->poster = $posterName;

        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $filmbyid = Film::find($id);
        return view('film.detail', ["filmbyid" => $filmbyid]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $filmbyid = Film::find($id);
        $genre = Genre::all();
        return view('film.edit', ["filmbyid" => $filmbyid, "genre" => $genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'poster' => 'mimes:jpg,jpeg,png|max:2048',
            'judul' => 'required',
            'genre_id' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
        ]);

        $filmbyid = Film::find($id);

        if ($request->has('poster')) {
            $path = "img/";
            File::delete($path . $filmbyid->poster);

            $posterName = time() . '.' . $request->poster->extension();

            $request->poster->move(public_path('img'), $posterName);

            $filmbyid->poster = $posterName;
        }

        $filmbyid->judul = $request['judul'];
        $filmbyid->ringkasan = $request['ringkasan'];
        $filmbyid->tahun = $request['tahun'];
        $filmbyid->genre_id = $request['genre_id'];

        $filmbyid->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $filmbyid = Film::find($id);
        $path = "img/";
        File::delete($path . $filmbyid->poster);

        $filmbyid->delete();

        return redirect('/film');
    }
}
