<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register()
    {
        return view("pages.register");
    }

    public function save(Request $request)
    {
        $fname = $request["first"];
        $lname = $request["last"];

        return view("pages.home", ["fname" => $fname, "lname" => $lname]);
    }
}
