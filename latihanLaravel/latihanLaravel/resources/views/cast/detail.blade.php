@extends('layouts.master')

@section('judul', 'Detail Category')

@section('content')
<h1 class="text-primary">{{ $castbyid ->nama }}</h1>
<h2 class="text-primary">Umur {{ $castbyid ->umur }}</h2>
<p>{{ $castbyid->bio }}</p>
@endsection
