@extends('layouts.master')

@section('judul', 'Register')

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>

<form action="/signup" method="post">
    @csrf
    <label for="first">First name:</label><br><br>
    <input type="text" name="first" id="first"><br><br>
    <label for="last">Last Name:</label><br><br>
    <input type="text" name="last" id="last"><br><br>
    <label for="gender">Gender:</label><br><br>
    <input type="radio" name="gender">Male <br>
    <input type="radio" name="gender">Female <br>
    <input type="radio" name="gender">Other <br><br>
    <label for="keewarganegaraan">Nationality:</label><br>
    <select name="kewarganegaraan">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia"> Malaysia</option>
        <option value="singapura">Singapura</option>
    </select><br><br>
    <label for="bahasa">Language Spoken</label><br><br>
    <input type="checkbox" name="b.indo" id="bahasa">Bahasa Indonesia <br>
    <input type="checkbox" name="english" id="bahasa">English <br>
    <input type="checkbox" name="other" id="bahasa">Other <br><br>
    <label for="bio">Bio:</label><br><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up">

</form>

@endsection
