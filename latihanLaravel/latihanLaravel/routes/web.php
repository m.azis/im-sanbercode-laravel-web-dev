<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, "index"]);

Route::get('/register', [RegisterController::class, "register"]);

Route::post('/signup', [RegisterController::class, "save"]);

Route::get('/dataTable', function () {
    return view('pages.dataTable');
});
Route::get('/table', function () {
    return view('pages.table');
});

//create data ->mengarahkan ke halaman form
Route::get('/cast/create', [CastController::class, "create"]);
//memasukkan data ke database
Route::post('/cast', [CastController::class, "store"]);

//read data
Route::get('/cast', [CastController::class, "index"]);

//menampilkan data berdasarkan id
Route::get('/cast/{id}', [CastController::class, "show"]);

//update
Route::get('/cast/{id}/edit', [CastController::class, "edit"]);

//mengubah data kedatabase berdasarkan id
Route::put('/cast/{id}', [CastController::class, "update"]);

//delete berdasarkan id
Route::delete('/cast/{id}', [CastController::class, "destroy"]);
