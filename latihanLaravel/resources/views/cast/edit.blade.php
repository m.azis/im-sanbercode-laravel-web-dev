@extends('layouts.master')

@section('judul', 'Edit')

@section('content')
<form action="/cast/{{ $castbyid->id }}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ $castbyid->nama }}">
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{ $castbyid->umur }}">
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control @error('bio') is-invalid @enderror" cols="30" rows="10">
        {{ $castbyid->bio }}
      </textarea>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
