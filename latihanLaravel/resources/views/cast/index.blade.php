@extends('layouts.master')

@section('judul', 'Cast')

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah Cast</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">umur</th>
        <th scope="col">aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $keys => $item)
        <tr>
            <th scope="row">{{ $keys + 1 }}</th>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td>
                <form action="/cast/{{ $item->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Update</a>
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </td>
          </tr>
        @empty
          <tr>
              <td>
                  Daftar Cast kosong
              </td>
          </tr>
        @endforelse

    </tbody>
  </table>
@endsection
