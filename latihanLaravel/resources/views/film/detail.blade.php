@extends('layouts.master')

@section('judul', 'Detail Film')

@section('content')
<h1>{{ $filmbyid->judul }}</h1>
<img src="{{ asset('/img/'. $filmbyid->poster ) }}" alt="" srcset="" class="rounded mx-auto d-block my-3">
<p>{{ $filmbyid->ringkasan }}</p>
<a href="/film" class="btn btn-info btn-sm">Kembali</a>

@endsection
