@extends('layouts.master')

@section('judul', 'Edit Film')

@section('content')
<form action="/film/{{ $filmbyid->id }}" method="POST" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul" value="{{ $filmbyid->judul }}">
          @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
    </div>
    <div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" class="form-control @error('ringkasan') is-invalid @enderror" cols="30" rows="10">{{ $filmbyid->ringkasan }}</textarea>
        @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label>
        <input type="number" class="form-control @error('tahun') is-invalid @enderror" name="tahun" value="{{ $filmbyid->tahun }}">
          @error('tahun')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
    </div>
    <div class="form-group">
        <label>Poster Film</label>
        <input type="file" class="form-control @error('poster') is-invalid @enderror" name="poster">
          @error('poster')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
    </div>
    <div class="form-group">
      <label>Genre</label>
      <select name="genre_id" class="form-control @error('genre_id') is-invalid @enderror">
            <option value="">--pilih Genre--</option>
        @forelse ($genre as $item)
            @if ($item->id === $filmbyid->genre_id)
            <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
            @else
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endif
        @empty
            <option value="">tidak ada genre</option>
        @endforelse
      </select>
        @error('genre_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
