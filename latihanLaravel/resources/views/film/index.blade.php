@extends('layouts.master')

@section('judul', 'Semua Film')

@section('content')
<a href="/film/create" class="btn btn-primary btn-sm my-3"> Tambah Film</a>
<div class="container">
    <div class="row">
        @forelse ($film as $item)
        <div class="col-4">
            <div class="card" >
                <img src="{{ asset('img/'.$item->poster) }}" style="height: 200px" class="card-img-top" alt="...">
                <div class="card-body">
                  <h2>{{ $item->judul }}</h2>
                  <p class="card-text">{{ Str::limit($item->ringkasan, 10) }}</p>
                  <a href="/film/{{ $item->id }}" class="btn btn-primary btn-sm btn-block">Read More</a>
                  <div class="row">
                      <div class="col my-3">
                        <a href="/film/{{ $item->id }}/edit" class="btn btn-warning btn-sm btn-block">Edit</a>
                      </div>
                      <div class="col my-3">
                        <form action="/film/{{ $item->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger btn-sm btn-block">Delete</button>
                        </form>
                      </div>
                  </div>
                </div>
              </div>
        </div>

        @empty

        @endforelse
    </div>
</div>
@endsection
