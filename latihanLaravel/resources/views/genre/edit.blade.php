@extends('layouts.master')

@section('judul', 'Edit')

@section('content')
<form action="/genre/{{ $genrebyid->id }}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ $genrebyid->nama }}">
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
