@extends('layouts.master')

@section('judul', 'Genre')

@section('content')
<a href="/genre/create" class="btn btn-primary btn-sm my-3">Tambah Cast</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $keys => $item)
        <tr>
            <th scope="row">{{ $keys + 1 }}</th>
            <td>{{ $item->nama }}</td>
            <td>
                <form action="/genre/{{ $item->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/genre/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/genre/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Update</a>
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </td>
          </tr>
        @empty
          <tr>
              <td>
                  Daftar Genre kosong
              </td>
          </tr>
        @endforelse

    </tbody>
  </table>
@endsection
