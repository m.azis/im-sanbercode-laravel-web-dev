@extends('layouts.master')

@section('judul', 'Tambah Peran')

@section('content')
<form action="/peran" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Cast</label>
      <select name="cast_id" class="form-control @error('cast_id') is-invalid @enderror">
            <option value="">--pilih cast--</option>
        @forelse ($cast as $item)
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @empty
            <option value="">tidak ada cast</option>
        @endforelse
      </select>
        @error('cast_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
      <label>Film</label>
      <select name="cast_id" class="form-control @error('cast_id') is-invalid @enderror">
            <option value="">--pilih Film--</option>
        @forelse ($cast as $item)
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @empty
            <option value="">tidak ada Film</option>
        @endforelse
      </select>
        @error('cast_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
