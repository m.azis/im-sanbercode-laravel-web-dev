<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");
echo "Name: " . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded: " . $sheep->cold_blooded . "<br> <br>"; // "no"

$buduk = new Frog("buduk");
echo "Name: " . $buduk->name . "<br>"; // "shaun"
echo "Legs: " . $buduk->legs . "<br>"; // 4
echo "Cold Blooded: " . $buduk->cold_blooded . "<br>"; // "no"
echo $buduk->yell() . "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->name . "<br>"; // "shaun"
echo "Legs: " . $sungokong->legs . "<br>"; // 4
echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>"; // "no"
echo $sungokong->yell(); // "Auooo"